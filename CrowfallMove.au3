#include <Misc.au3>
#include <GUIConstants.au3>
#include <ColorConstants.au3>

;~ TODO List:
;~ - If caps lock is on and macro is sending it will spam caps lock on and off (most likley because it is trying to send a lower case char)

AutoItSetOption("SendCapslockMode", 0)

createGui()

Global $pause = True
Global $reset = False
HotKeySet("`", "togglePause")

;~ How long a button takes to become not pressed (and therefore not held down
Const $pressValue = 20
;~ Sleep at the end of the loop (1 = 1ms wait between iterations)
Const $commandSpeed = 1
;~ How long a button needs to be pressed before we continue to hold it down when it is released
Const $triggerDuration = 50

;~ Release any held down buttons.  Needs to be different than reset because we don't want to reset active buttons
;~ but when we press a new button we should release any held buttons
Local $release = False
Local $pressDuration = 0
Local $wIsPressed = 0
Local $aIsPressed = 0
Local $sIsPressed = 0
Local $dIsPressed = 0

Local $hDLL = DllOpen("user32.dll")

;~ Local $hWnd = WinWait("[CLASS:Notepad]", "", 10)

;~ W->57, A->41, S->53, D->44
While 1
   If $reset Then
	  $wIsPressed = 0
	  $aIsPressed = 0
	  $sIsPressed = 0
	  $dIsPressed = 0
	  $reset = False
   EndIf
   If $pause Then
	  ContinueLoop
   EndIf
   If _IsPressed("57", $hDLL) Or _IsPressed("41", $hDLL) Or _IsPressed("53", $hDLL) Or _IsPressed("44", $hDLL) Then
	  If $release Then
		 $reset = True
		 $release = False
		 $pressDuration = 0
	  EndIf

	  If _IsPressed("57", $hDLL) Then
		 $wIsPressed = $pressValue
	  EndIf
	  If _IsPressed("41", $hDLL) Then
		 $aIsPressed = $pressValue
	  EndIf
	  If _IsPressed("53", $hDLL) Then
		 $sIsPressed = $pressValue
	  EndIf
	  If _IsPressed("44", $hDLL) Then
		 $dIsPressed = $pressValue
	  EndIf

	  If $wIsPressed > 0 Then
		 $wIsPressed = $wIsPressed - 1
	  EndIf
	  If $aIsPressed > 0 Then
		 $aIsPressed = $aIsPressed - 1
	  EndIf
	  If $sIsPressed > 0 Then
		 $sIsPressed = $sIsPressed - 1
	  EndIf
	  If $dIsPressed > 0 Then
		 $dIsPressed = $dIsPressed - 1
	  EndIf

	  If $pressDuration <= $triggerDuration Then
		 $pressDuration = $pressDuration + 1
	  EndIf
   ElseIf $pressDuration > $triggerDuration Then
	  $release = True
	  if $wIsPressed > 0 Then
		 Send("w");~ Send("{a down}") ; Holds the A key down
	  EndIf
	  if $aIsPressed > 0 Then
		 Send("a")
	  EndIf
	  if $sIsPressed > 0 Then
		 Send("s")
	  EndIf
	  if $dIsPressed > 0 Then
		 Send("s")
	  EndIf
   EndIf

   sleep($commandSpeed)
WEnd

Func togglePause()
   $pause = Not $pause
   $reset = True

   If $pause Then
	  GUISetBkColor($COLOR_RED)
   Else
	  GUISetBkColor($COLOR_GREEN)
   EndIf
EndFunc

Func createGui()
   Local $wHnd = GUICreate("", 50, 50, 0, 0, $WS_POPUP, BitOR($WS_EX_TOPMOST, $WS_EX_TOOLWINDOW))
   GUISetState(@SW_SHOW, $wHnd)
   GUISetBkColor($COLOR_RED)
EndFunc

DllClose($hDLL)