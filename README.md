# Wasd Helper

Simple au3 script to make wasd movement continue in last direction after keys are released.  This is useful if multiple hotkeys need to be managed but you still want your character to be mobile.

# Setup

Released versions can be found in the download section [here](https://bitbucket.org/Pyetrek/wasd-helper/downloads/)

# Use

The scripts starts in the paused state and should not effect anything.  Use the `` ` `` to unpause the script.  The square in the top left corner should go green to indicate it is on.  Pressing the key again should pause it.  To close the script simply left click on the icon in the system tray (bottom right corner) and click exit.  Once the script is running it will behave as follows for the `WASD` keys:

* Clicking any of the movement keys will work as normal
* Holding down the keys for a short duration and releasing wil result in the keys being held down, and as a result you character should continue moving in that direction.

# Troubleshooting

If you notice the macro not functioning correctly, check the system tray and make sure the `Script Paused` option is not checked.  It will get checked when you hover over the icon for the first time.